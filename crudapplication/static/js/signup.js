$("#loginForm").validate({
    messages:{
        username:{
            required: "Please enter a valid username."
        },
        password:{
            required: "Please enter a password."
        }
    }
});

$("#signup").validate({
    rules: {
        phone: {
            digits: true
        }
    }
});

$("#editProfile").validate({
    rules:{
        contact_number:{
            digits: true
        }
    }
});