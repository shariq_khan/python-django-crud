# <a>Django Crud - Login, Register,Dashboard Etc </a>

### Live demo <a target="_blank" href="http://shariqkhan1.pythonanywhere.com/">Click Here</a>
##### username : test.user , password : kiwitech



#### Key Features
- 1 . Register
- 2 . Login
- 3 . File Upload
- 4 . Form Validation
- 5 . CRUD Application
- 6 . Display number of times the user has logged in
- 7 . Display number of times the user has logged out

# Steps for Set Up
``` 
 1. git clone https://gitlab.com/shariq_khan/python-django-crud.git

 2. Change settings.py MYSQL CONFIGURATIONS (name, user, password) and SECRET_KEY

 3. cd python-django-crud
 
 4. python3 -m venv ./venv
 
 5. source ./venv/bin/activate

 6. pip install -r requirements.txt

 7. python manage.py migrate

 8. python manage.py runserver

 9. Login to http://127.0.0.1:8000

```