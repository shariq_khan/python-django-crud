from django.shortcuts import render, redirect
from django.contrib import messages, auth
from django.contrib.auth.models import User
from django.http import HttpResponse
from usermanagement.models import UserProfile
import datetime

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            userProfile = UserProfile.objects.filter(user_id=user.id).get()
            numberOfLogins = 1
            if userProfile.no_of_logins is not None:
                numberOfLogins = userProfile.no_of_logins + 1
            userProfile.no_of_logins = numberOfLogins
            userProfile.save()
            messages.success(request, 'You are now logged in')
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid credentials')
            return redirect('login')
    else:
        return render(request, 'pages/login.html')

def signup(request):
    if request.method == 'POST':
        #Get Form values
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        password2 = request.POST['password2']
        phone_number = request.POST['phone']
        profile_picture = request.FILES['image']
        # check if password match
        if password == password2:
            # check username
            if User.objects.filter(username=username).exists():
                messages.error(request, 'That username is taken')
                return redirect('signup')
            else:
                # check email
                if User.objects.filter(email=email).exists():
                    messages.error(request, 'That email is being used')
                    return redirect('signup')
                if UserProfile.objects.filter(phone_number=phone_number).exists():
                    messages.error(request, 'That phone number is being used')
                    return redirect('signup')
                else:
                    user = User.objects.create_user(username=username, password=password, email=email, first_name=first_name, last_name=last_name)
                    profile = UserProfile(phone_number=phone_number,picture=profile_picture,user_id=user.id)
                    profile.save()
                    messages.success(request, 'You are now registered and can log in')
                    return redirect('signup')
        else:
            messages.error(request, 'Password do not match')
            return redirect('signup')
    else:
        return render(request, 'pages/signup.html')

def dashboard(request):
    userProfile = UserProfile.objects.filter(user_id=request.user.id).get()
    context = {
        "userProfile" : userProfile
    }
    return render(request, 'pages/dashboard.html', context)

def logout(request):
    if request.method == 'POST':
        userProfile = UserProfile.objects.filter(user_id=request.user.id).get()
        userProfile.last_logout_time = datetime.datetime.now()
        userProfile.save()
        auth.logout(request)
        messages.success(request, 'You are now logged out')
        return redirect('login')

def editProfile(request):
    if request.method == 'POST':
        #Get Form values
        first_name = request.POST['fname']
        last_name = request.POST['lname']
        email = request.POST['email']
        phone_number = request.POST['contact_number']
        if User.objects.filter(email=email).exclude(id=request.user.id).exists():
            messages.error(request, 'That email address is taken')
            return redirect('editProfile')
        if UserProfile.objects.filter(phone_number=phone_number).exclude(user_id=request.user.id).exists():
            messages.error(request, 'That phone number is being used')
            return redirect('editProfile')
        User.objects.filter(id=request.user.id).update(first_name=first_name, last_name = last_name, email = email)
        userProfile = UserProfile.objects.filter(user_id=request.user.id).get()
        userProfile.phone_number = phone_number
        if 'image' in request.FILES:
            userProfile.picture = request.FILES['image']
        userProfile.save()
        messages.success(request, 'Details updated successfully')
        return redirect('dashboard')
    else:
        userProfile = UserProfile.objects.filter(user_id=request.user.id).get()
        context = {
            "userProfile" : userProfile
        }
        return render(request, 'pages/editProfile.html', context)