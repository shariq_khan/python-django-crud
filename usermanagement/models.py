from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    phone_number =  models.CharField(max_length=10)
    last_logout_time = models.DateTimeField(null=True, blank=True)
    no_of_logins = models.IntegerField(null=True, blank=True)
    picture = models.ImageField(upload_to='photos', blank=True, null=True)